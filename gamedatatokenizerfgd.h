#ifndef GAMEDATATOKENIZER_H
#define GAMEDATATOKENIZER_H

#include <istream>
#include <string.h>
#include <functional>
#include <queue>

#define CaseInsensitiveCompare(s1, s2)	!strcmpi(s1.c_str(), s2)

enum class GameDataTokenTypes
{
    EndOfFile,
    String,
    Number,
    Identifier,
    Operator,
    Error,
};

class StashedToken
{
public:
    StashedToken(GameDataTokenTypes type, const std::string& token);

    GameDataTokenTypes _type;
    std::string _token;
};

class GameDataTokenizerFGD
{
    std::istream& _stream;
    int _line;
    GameDataTokenTypes _lastType;
    std::function<void (const std::string&, int)> _error;
    std::queue<StashedToken> _stash;
public:
    GameDataTokenizerFGD(std::istream& stream);
    virtual ~GameDataTokenizerFGD();

    GameDataTokenTypes NextToken(std::string& token);
    bool NextTokenCheck(GameDataTokenTypes checkType, const std::string& checkToken, bool stashOnFalse = false);
    bool NextTokenCheckType(GameDataTokenTypes checkType, std::string& checkToken);
    GameDataTokenTypes GetStringToken(std::string& token);
    void SkipWhiteSpace();
    void SkipCommentLine();
    void SkipUntill(const char a);
    bool EndOfLine();

    int CurrentLine() const { return this->_line; }

    void SetErrorHandler(std::function<void (const std::string&, int)> error);
    bool Error(const std::string& error, bool result = false);

    void StashToken(GameDataTokenTypes type, const std::string& token);
};

#endif // GAMEDATATOKENIZER_H
