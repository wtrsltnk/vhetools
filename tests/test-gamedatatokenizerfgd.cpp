#include "catch.hpp"
#include <iostream>
#include <sstream>
#include <vector>

#include "../gamedata.h"
#include "../gamedatatokenizerfgd.h"

TEST_CASE("Test tokenizer operator", "[gamedatatokenizer]")
{
    std::istringstream ss("test @ -5 [test2] \"test string\"");
    auto tok = new GameDataTokenizerFGD(ss);
    std::string token;

    REQUIRE(tok->NextToken(token) == GameDataTokenTypes::Identifier);
    REQUIRE(token == "test");

    REQUIRE(tok->NextToken(token) == GameDataTokenTypes::Operator);
    REQUIRE(token == "@");

    REQUIRE(tok->NextToken(token) == GameDataTokenTypes::Number);
    REQUIRE(token == "-5");

    REQUIRE(tok->NextToken(token) == GameDataTokenTypes::Operator);
    REQUIRE(token == "[");

    REQUIRE(tok->NextToken(token) == GameDataTokenTypes::Identifier);
    REQUIRE(token == "test2");

    REQUIRE(tok->NextToken(token) == GameDataTokenTypes::Operator);
    REQUIRE(token == "]");

    REQUIRE(tok->NextToken(token) == GameDataTokenTypes::String);
    REQUIRE(token == "test string");
}

TEST_CASE("Test tokenizer invalid characters after number", "[gamedatatokenizer]")
{
    std::istringstream ss("test -575behold");
    auto tok = new GameDataTokenizerFGD(ss);
    std::string token;

    REQUIRE(tok->NextToken(token) == GameDataTokenTypes::Identifier);
    REQUIRE(token == "test");

    REQUIRE(tok->NextToken(token) == GameDataTokenTypes::Error);
}

TEST_CASE("Test tokenizer with comment", "[gamedatatokenizer]")
{
    std::istringstream ss("test // test comment\n\"test new line\"");
    auto tok = new GameDataTokenizerFGD(ss);
    std::string token;

    REQUIRE(tok->NextToken(token) == GameDataTokenTypes::Identifier);
    REQUIRE(token == "test");

    REQUIRE(tok->NextToken(token) == GameDataTokenTypes::String);
    REQUIRE(token == "test new line");
}

TEST_CASE("Test stashing tokens", "[gamedatatokenizer]")
{
    std::istringstream ss("test // test comment\n\"test new line\"");
    auto tok = new GameDataTokenizerFGD(ss);
    std::string token;

    REQUIRE(tok->NextToken(token) == GameDataTokenTypes::Identifier);
    REQUIRE(token == "test");

    tok->StashToken(GameDataTokenTypes::Identifier, token);

    REQUIRE(tok->NextToken(token) == GameDataTokenTypes::Identifier);
    REQUIRE(token == "test");
}
