#include "catch.hpp"
#include <iostream>
#include <vector>

#include "../gameconfig.h"

std::vector<GameConfig*> setupStuff()
{
    std::vector<GameConfig*> configs;

    auto config1 = new GameConfig();
    config1->setBaseDir("BaseDir 1");
    config1->setBspDir("BspDir 1");
    config1->setBspExe("BspExe 1");
    config1->setCsgExe("CsgExe 1");
    config1->setDefaultPointEntity("DefaultPointEntity 1");
    config1->setDefaultSolidEntity("DefaultSolidEntity 1");
    config1->setGameExe("GameExe 1");
    config1->setGameExeDir("GameExeDir 1");
    config1->setMapFormat(GameConfigMapFormats::HalfLife);
    config1->setModDir("ModDir 1");
    config1->setName("Name 1");
    config1->setPaletteFile("PaletteFile 1");
    config1->setRadExe("RadExe 1");
    config1->setRmfDir("RmfDir 1");
    config1->setTextureFormat(GameConfigTextureFormats::WAD3);
    config1->setVisExe("VisExe 1");
    configs.push_back(config1);

    auto config2 = new GameConfig();
    config2->setBaseDir("BaseDir 2");
    config2->setBspDir("BspDir 2");
    config2->setBspExe("BspExe 2");
    config2->setCsgExe("CsgExe 2");
    config2->setDefaultPointEntity("DefaultPointEntity 2");
    config2->setDefaultSolidEntity("DefaultSolidEntity 2");
    config2->setGameExe("GameExe 2");
    config2->setGameExeDir("GameExeDir 2");
    config2->setMapFormat(GameConfigMapFormats::HalfLife2);
    config2->setModDir("ModDir 2");
    config2->setName("Name 2");
    config2->setPaletteFile("PaletteFile 2");
    config2->setRadExe("RadExe 2");
    config2->setRmfDir("RmfDir 2");
    config2->setTextureFormat(GameConfigTextureFormats::WAD4);
    config2->setVisExe("VisExe 2");
    configs.push_back(config2);

    return configs;
}

TEST_CASE("Test file writing and reading of game config version 1.4", "[gameconfig]")
{
    auto configs = setupStuff();

    try
    {
        GameConfig::WriteGameConfigFile("gameconfig.wc", configs, 1.4f);
        auto readConfigs = GameConfig::ReadGameConfigFile("gameconfig.wc");

        REQUIRE(configs.size() == readConfigs.size());
        for (int i = 0; i < configs.size(); i++)
        {
            REQUIRE(std::string(configs[i]->baseDir()) == std::string(readConfigs[i]->baseDir()));
            REQUIRE(std::string(configs[i]->bspDir()) == std::string(readConfigs[i]->bspDir()));
            REQUIRE(std::string(configs[i]->bspExe()) == std::string(readConfigs[i]->bspExe()));
            REQUIRE(std::string(configs[i]->csgExe()) == std::string(readConfigs[i]->csgExe()));
            REQUIRE(std::string(configs[i]->defaultPointEntity()) == std::string(readConfigs[i]->defaultPointEntity()));
            REQUIRE(std::string(configs[i]->defaultSolidEntity()) == std::string(readConfigs[i]->defaultSolidEntity()));
            REQUIRE(std::string(configs[i]->gameExe()) == std::string(readConfigs[i]->gameExe()));
            REQUIRE(std::string(configs[i]->gameExeDir()) == std::string(readConfigs[i]->gameExeDir()));
            REQUIRE(configs[i]->mapFormat() == readConfigs[i]->mapFormat());
            REQUIRE(std::string(configs[i]->modDir()) == std::string(readConfigs[i]->modDir()));
            REQUIRE(std::string(configs[i]->name()) == std::string(readConfigs[i]->name()));
            // Palette file is not written or read on files with version >= 1.4
            REQUIRE(std::string(readConfigs[i]->paletteFile()) == std::string(""));
            REQUIRE(std::string(configs[i]->radExe()) == std::string(readConfigs[i]->radExe()));
            REQUIRE(std::string(configs[i]->rmfDir()) == std::string(readConfigs[i]->rmfDir()));
            REQUIRE(configs[i]->textureFormat() == readConfigs[i]->textureFormat());
            REQUIRE(std::string(configs[i]->visExe()) == std::string(readConfigs[i]->visExe()));
        }
    }
    catch (GameConfigException* ex)
    {
        std::cout << "GameConfigException : " << ex->msg << std::endl;
    }
}

TEST_CASE("Test file writing and reading of game config version 1.3", "[gameconfig]")
{
    auto configs = setupStuff();

    try
    {
        GameConfig::WriteGameConfigFile("gameconfig.wc", configs, 1.3f);
        auto readConfigs = GameConfig::ReadGameConfigFile("gameconfig.wc");

        REQUIRE(configs.size() == readConfigs.size());
        for (int i = 0; i < configs.size(); i++)
        {
            REQUIRE(std::string("") == std::string(readConfigs[i]->baseDir()));
            REQUIRE(std::string(configs[i]->bspDir()) == std::string(readConfigs[i]->bspDir()));
            REQUIRE(std::string(configs[i]->bspExe()) == std::string(readConfigs[i]->bspExe()));
            REQUIRE(std::string("") == std::string(readConfigs[i]->csgExe()));
            REQUIRE(std::string(configs[i]->defaultPointEntity()) == std::string(readConfigs[i]->defaultPointEntity()));
            REQUIRE(std::string(configs[i]->defaultSolidEntity()) == std::string(readConfigs[i]->defaultSolidEntity()));
            REQUIRE(std::string(configs[i]->gameExe()) == std::string(readConfigs[i]->gameExe()));
            REQUIRE(std::string(configs[i]->gameExeDir()) == std::string(readConfigs[i]->gameExeDir()));
            REQUIRE(configs[i]->mapFormat() == readConfigs[i]->mapFormat());
            REQUIRE(std::string("") == std::string(readConfigs[i]->modDir()));
            REQUIRE(std::string(configs[i]->name()) == std::string(readConfigs[i]->name()));
            REQUIRE(std::string(configs[i]->paletteFile()) == std::string(readConfigs[i]->paletteFile()));
            REQUIRE(std::string(configs[i]->radExe()) == std::string(readConfigs[i]->radExe()));
            REQUIRE(std::string(configs[i]->rmfDir()) == std::string(readConfigs[i]->rmfDir()));
            REQUIRE(configs[i]->textureFormat() == readConfigs[i]->textureFormat());
            REQUIRE(std::string(configs[i]->visExe()) == std::string(readConfigs[i]->visExe()));
        }
    }
    catch (GameConfigException* ex)
    {
        std::cout << "GameConfigException : " << ex->msg << std::endl;
    }
}

TEST_CASE("Test file writing and reading of game config version 1.2", "[gameconfig]")
{
    auto configs = setupStuff();

    try
    {
        GameConfig::WriteGameConfigFile("gameconfig.wc", configs, 1.2f);
        auto readConfigs = GameConfig::ReadGameConfigFile("gameconfig.wc");

        REQUIRE(configs.size() == readConfigs.size());
        for (int i = 0; i < configs.size(); i++)
        {
            REQUIRE(std::string("") == std::string(readConfigs[i]->baseDir()));
            REQUIRE(std::string("") == std::string(readConfigs[i]->bspDir()));
            REQUIRE(std::string(configs[i]->bspExe()) == std::string(readConfigs[i]->bspExe()));
            REQUIRE(std::string("") == std::string(readConfigs[i]->csgExe()));
            REQUIRE(std::string(configs[i]->defaultPointEntity()) == std::string(readConfigs[i]->defaultPointEntity()));
            REQUIRE(std::string(configs[i]->defaultSolidEntity()) == std::string(readConfigs[i]->defaultSolidEntity()));
            REQUIRE(std::string(configs[i]->gameExe()) == std::string(readConfigs[i]->gameExe()));
            REQUIRE(std::string(configs[i]->gameExeDir()) == std::string(readConfigs[i]->gameExeDir()));
            REQUIRE(configs[i]->mapFormat() == readConfigs[i]->mapFormat());
            REQUIRE(std::string("") == std::string(readConfigs[i]->modDir()));
            REQUIRE(std::string(configs[i]->name()) == std::string(readConfigs[i]->name()));
            REQUIRE(std::string(configs[i]->paletteFile()) == std::string(readConfigs[i]->paletteFile()));
            REQUIRE(std::string(configs[i]->radExe()) == std::string(readConfigs[i]->radExe()));
            REQUIRE(std::string(configs[i]->rmfDir()) == std::string(readConfigs[i]->rmfDir()));
            REQUIRE(configs[i]->textureFormat() == readConfigs[i]->textureFormat());
            REQUIRE(std::string(configs[i]->visExe()) == std::string(readConfigs[i]->visExe()));
        }
    }
    catch (GameConfigException* ex)
    {
        std::cout << "GameConfigException : " << ex->msg << std::endl;
    }
}

TEST_CASE("Test file writing and reading of game config version 1.1", "[gameconfig]")
{
    auto configs = setupStuff();

    try
    {
        GameConfig::WriteGameConfigFile("gameconfig.wc", configs, 1.1f);
        auto readConfigs = GameConfig::ReadGameConfigFile("gameconfig.wc");

        REQUIRE(configs.size() == readConfigs.size());
        for (int i = 0; i < configs.size(); i++)
        {
            REQUIRE(std::string("") == std::string(readConfigs[i]->baseDir()));
            REQUIRE(std::string("") == std::string(readConfigs[i]->bspDir()));
            REQUIRE(std::string("") == std::string(readConfigs[i]->bspExe()));
            REQUIRE(std::string("") == std::string(readConfigs[i]->csgExe()));
            REQUIRE(std::string(configs[i]->defaultPointEntity()) == std::string(readConfigs[i]->defaultPointEntity()));
            REQUIRE(std::string(configs[i]->defaultSolidEntity()) == std::string(readConfigs[i]->defaultSolidEntity()));
            REQUIRE(std::string(configs[i]->gameExe()) == std::string(readConfigs[i]->gameExe()));
            REQUIRE(std::string("") == std::string(readConfigs[i]->gameExeDir()));
            REQUIRE(configs[i]->mapFormat() == readConfigs[i]->mapFormat());
            REQUIRE(std::string("") == std::string(readConfigs[i]->modDir()));
            REQUIRE(std::string(configs[i]->name()) == std::string(readConfigs[i]->name()));
            REQUIRE(std::string(configs[i]->paletteFile()) == std::string(readConfigs[i]->paletteFile()));
            REQUIRE(std::string("") == std::string(readConfigs[i]->radExe()));
            REQUIRE(std::string("") == std::string(readConfigs[i]->rmfDir()));
            REQUIRE(configs[i]->textureFormat() == readConfigs[i]->textureFormat());
            REQUIRE(std::string("") == std::string(readConfigs[i]->visExe()));
        }
    }
    catch (GameConfigException* ex)
    {
        std::cout << "GameConfigException : " << ex->msg << std::endl;
    }
}

TEST_CASE("Test file writing and reading of game config version 1.0", "[gameconfig]")
{
    auto configs = setupStuff();

    try
    {
        GameConfig::WriteGameConfigFile("gameconfig.wc", configs, 1.0f);
        auto readConfigs = GameConfig::ReadGameConfigFile("gameconfig.wc");

        REQUIRE(configs.size() == readConfigs.size());
        for (int i = 0; i < configs.size(); i++)
        {
            REQUIRE(std::string("") == std::string(readConfigs[i]->baseDir()));
            REQUIRE(std::string("") == std::string(readConfigs[i]->bspDir()));
            REQUIRE(std::string("") == std::string(readConfigs[i]->bspExe()));
            REQUIRE(std::string("") == std::string(readConfigs[i]->csgExe()));
            REQUIRE(std::string(configs[i]->defaultPointEntity()) == std::string(readConfigs[i]->defaultPointEntity()));
            REQUIRE(std::string(configs[i]->defaultSolidEntity()) == std::string(readConfigs[i]->defaultSolidEntity()));
            REQUIRE(std::string(configs[i]->gameExe()) == std::string(readConfigs[i]->gameExe()));
            REQUIRE(std::string("") == std::string(readConfigs[i]->gameExeDir()));
            REQUIRE(GameConfigMapFormats::Quake == readConfigs[i]->mapFormat());
            REQUIRE(std::string("") == std::string(readConfigs[i]->modDir()));
            REQUIRE(std::string(configs[i]->name()) == std::string(readConfigs[i]->name()));
            REQUIRE(std::string(configs[i]->paletteFile()) == std::string(readConfigs[i]->paletteFile()));
            REQUIRE(std::string("") == std::string(readConfigs[i]->radExe()));
            REQUIRE(std::string("") == std::string(readConfigs[i]->rmfDir()));
            REQUIRE(configs[i]->textureFormat() == readConfigs[i]->textureFormat());
            REQUIRE(std::string("") == std::string(readConfigs[i]->visExe()));
        }
    }
    catch (GameConfigException* ex)
    {
        std::cout << "GameConfigException : " << ex->msg << std::endl;
    }
}
