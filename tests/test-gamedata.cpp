#include "catch.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "../gamedata.h"
#include "../gamedataclass.h"
#include "../gamedatatokenizerfgd.h"

TEST_CASE("Test reading PointClass", "[gamedata]")
{
    std::istringstream ss("@PointClass base(Targetname) iconsprite(\"sprites/test.spr\") = info_null : \"info_null (spotlight target)\" []");
    auto tok = GameDataTokenizerFGD(ss);
    tok.SetErrorHandler([] (const std::string& message, int line) {
        std::cout << "ERROR on line " << line << " : " << message << std::endl;
    });

    auto gameData = GameData();
    auto result = gameData.ReadFGD(tok);

    REQUIRE(result == true);
    REQUIRE(gameData._classes.size() == 1);
    REQUIRE(gameData._classes[0]->_classType == GameDataClassTypes::PointClass);
    REQUIRE(gameData._classes[0]->_inherited.size() == 1);
    REQUIRE(gameData._classes[0]->_inherited[0] == "Targetname");
    REQUIRE(gameData._classes[0]->_editorIconSprite == "sprites/test.spr");
    REQUIRE(gameData._classes[0]->_name == "info_null");
    REQUIRE(gameData._classes[0]->_description == "info_null (spotlight target)");
}

TEST_CASE("Test reading two classes", "[gamedata]")
{
    std::istringstream ss("@PointClass = info_null : \"info_null (spotlight target)\" []\
@solidclass = func_wall : \"func_wall (Solid)\" []");
    auto tok = GameDataTokenizerFGD(ss);
    tok.SetErrorHandler([] (const std::string& message, int line) {
        std::cout << "ERROR on line " << line << " : " << message << std::endl;
    });

    auto gameData = GameData();
    auto result = gameData.ReadFGD(tok);

    REQUIRE(result == true);
    REQUIRE(gameData._classes.size() == 2);
    REQUIRE(gameData._classes[0]->_classType == GameDataClassTypes::PointClass);
    REQUIRE(gameData._classes[0]->_name == "info_null");
    REQUIRE(gameData._classes[0]->_description == "info_null (spotlight target)");
    REQUIRE(gameData._classes[1]->_classType == GameDataClassTypes::SolidClass);
    REQUIRE(gameData._classes[1]->_name == "func_wall");
    REQUIRE(gameData._classes[1]->_description == "func_wall (Solid)");
}

TEST_CASE("Test reading classes with size and color", "[gamedata]")
{
    std::istringstream ss("@PointClass size(-1 -2 -3, 4 5 6) color(7 8 9) = info_null []");
    auto tok = GameDataTokenizerFGD(ss);
    tok.SetErrorHandler([] (const std::string& message, int line) {
        std::cout << "ERROR on line " << line << " : " << message << std::endl;
    });

    auto gameData = GameData();
    auto result = gameData.ReadFGD(tok);

    REQUIRE(result == true);
    REQUIRE(gameData._classes.size() == 1);
    REQUIRE(gameData._classes[0]->_classType == GameDataClassTypes::PointClass);
    REQUIRE(gameData._classes[0]->_name == "info_null");
    REQUIRE(gameData._classes[0]->_description == "");
    REQUIRE(gameData._classes[0]->_editorSize.mins[0] == -1);
    REQUIRE(gameData._classes[0]->_editorSize.mins[1] == -2);
    REQUIRE(gameData._classes[0]->_editorSize.mins[2] == -3);
    REQUIRE(gameData._classes[0]->_editorSize.maxs[0] == 4);
    REQUIRE(gameData._classes[0]->_editorSize.maxs[1] == 5);
    REQUIRE(gameData._classes[0]->_editorSize.maxs[2] == 6);
    REQUIRE(gameData._classes[0]->_editorColor.rgb[0] == 7);
    REQUIRE(gameData._classes[0]->_editorColor.rgb[1] == 8);
    REQUIRE(gameData._classes[0]->_editorColor.rgb[2] == 9);
}

TEST_CASE("Test reading  studio and sprite classes", "[gamedata]")
{
    std::istringstream ss("@PointClass studio() = info_null []\
@PointClass sprite() = info_null []\
@PointClass = info_null []");
    auto tok = GameDataTokenizerFGD(ss);
    tok.SetErrorHandler([] (const std::string& message, int line) {
        std::cout << "ERROR on line " << line << " : " << message << std::endl;
    });

    auto gameData = GameData();
    auto result = gameData.ReadFGD(tok);

    REQUIRE(result == true);
    REQUIRE(gameData._classes.size() == 3);
    REQUIRE(gameData._classes[0]->_isSprite == false);
    REQUIRE(gameData._classes[0]->_isStudio == true);
    REQUIRE(gameData._classes[1]->_isSprite == true);
    REQUIRE(gameData._classes[1]->_isStudio == false);
    REQUIRE(gameData._classes[2]->_isSprite == false);
    REQUIRE(gameData._classes[2]->_isStudio == false);
}

TEST_CASE("Test reading whole file", "[gamedata]")
{
    std::ifstream ss("../hlfix/tests/halflife-cs.fgd");

    REQUIRE(ss.good() == true);

    auto tok = GameDataTokenizerFGD(ss);
    tok.SetErrorHandler([] (const std::string& message, int line) {
        std::cout << "ERROR on line " << line << " : " << message << std::endl;
    });

    auto gameData = GameData();
    auto result = gameData.ReadFGD(tok);

    REQUIRE(result == true);
    REQUIRE(gameData._classes.size() == 100);
}
