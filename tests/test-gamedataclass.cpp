#include "catch.hpp"
#include <iostream>
#include <sstream>
#include <vector>

#include "../gamedataclass.h"
#include "../gamedataproperty.h"
#include "../gamedatatokenizerfgd.h"

TEST_CASE("Test read parameters from FGD", "[gamedataclass]")
{
    std::istringstream ss("(test1, test2, test3)");
    auto tok = GameDataTokenizerFGD(ss);
    tok.SetErrorHandler([] (const std::string& message, int line) {
        std::cout << "ERROR on line " << line << " : " << message << std::endl;
    });

    std::vector<std::string> parameters;
    auto result = GameDataClass::ReadParameterListFromFGD(tok, parameters);

    REQUIRE(result == true);

    REQUIRE(parameters.size() == 3);
    REQUIRE(parameters[0] == "test1");
    REQUIRE(parameters[1] == "test2");
    REQUIRE(parameters[2] == "test3");
}

TEST_CASE("Test read two vec3 parameters from FGD", "[gamedataclass]")
{
    std::istringstream ss("(1 2 3, 4 5 6)");
    auto tok = GameDataTokenizerFGD(ss);
    tok.SetErrorHandler([] (const std::string& message, int line) {
        std::cout << "ERROR on line " << line << " : " << message << std::endl;
    });

    std::vector<std::string> parameters;
    auto result = GameDataClass::ReadParameterListFromFGD(tok, parameters);

    REQUIRE(result == true);

    REQUIRE(parameters.size() == 2);
    REQUIRE(parameters[0] == "1 2 3");
    REQUIRE(parameters[1] == "4 5 6");
}

TEST_CASE("Test read String parameter from FGD", "[gamedataclass]")
{
    std::istringstream ss("(\"Test String\")");
    auto tok = GameDataTokenizerFGD(ss);
    tok.SetErrorHandler([] (const std::string& message, int line) {
        std::cout << "ERROR on line " << line << " : " << message << std::endl;
    });

    std::vector<std::string> parameters;
    auto result = GameDataClass::ReadParameterListFromFGD(tok, parameters);

    REQUIRE(result == true);

    REQUIRE(parameters.size() == 1);
    REQUIRE(parameters[0] == "Test String");
}

TEST_CASE("Test read property", "[gamedataclass]")
{
    std::istringstream ss("(string) : \"Delay before trigger\" : \"0\"");
    auto tok = GameDataTokenizerFGD(ss);
    tok.SetErrorHandler([] (const std::string& message, int line) {
        std::cout << "ERROR on line " << line << " : " << message << std::endl;
    });

    auto prop = GameDataProperty("delay");
    REQUIRE(prop._name == "delay");

    auto result = prop.ReadPropertyFromFGD(tok);

    REQUIRE(result == true);
    REQUIRE(prop._propertyType == GameDataPropertyTypes::String);
    REQUIRE(prop._description == "Delay before trigger");
    REQUIRE(prop._defaultString == "0");
}

TEST_CASE("Test read choices property", "[gamedataclass]")
{
    std::istringstream ss("(choices) : \"Trigger State\" : 2 = \
                          [\
                              0: \"Off\"\
                              1: \"On\"\
                              2: \"Toggle\"\
                          ]");
    auto tok = GameDataTokenizerFGD(ss);
    tok.SetErrorHandler([] (const std::string& message, int line) {
        std::cout << "ERROR on line " << line << " : " << message << std::endl;
    });

    auto prop = GameDataProperty("triggerstate");
    REQUIRE(prop._name == "triggerstate");

    auto result = prop.ReadPropertyFromFGD(tok);

    REQUIRE(result == true);
    REQUIRE(prop._propertyType == GameDataPropertyTypes::Choices);
    REQUIRE(prop._defaultInteger.find(2) != prop._defaultInteger.end());
    REQUIRE(prop._options.size() == 3);
    REQUIRE(prop._options[0] == "Off");
    REQUIRE(prop._options[1] == "On");
    REQUIRE(prop._options[2] == "Toggle");
}

TEST_CASE("Test read flags property", "[gamedataclass]")
{
    std::istringstream ss("(flags) = \
                          [ \
                              1: \"Target Once\" : 0 \
                              2: \"Start Off\" : 0\
                              16:\"FireClientOnly\" : 0\
                              32:\"TouchClientOnly\" : 0\
                          ]");
    auto tok = GameDataTokenizerFGD(ss);
    tok.SetErrorHandler([] (const std::string& message, int line) {
        std::cout << "ERROR on line " << line << " : " << message << std::endl;
    });

    auto prop = GameDataProperty("spawnflags");
    REQUIRE(prop._name == "spawnflags");

    auto result = prop.ReadPropertyFromFGD(tok);

    REQUIRE(result == true);
    REQUIRE(prop._propertyType == GameDataPropertyTypes::Flags);
    REQUIRE(prop._options.size() == 4);
}

TEST_CASE("Test class with two properties", "[gamedataclass]")
{
    std::istringstream ss("= worldspawn : \"World entity\"\
[\
    message(string) : \"Map Description / Title\"\
    skyname(string) : \"environment map (cl_skyname)\"\
    light(integer) : \"Default light level\"\
    WaveHeight(string) : \"Default Wave Height\"\
    MaxRange(string) : \"Max viewable distance\" : \"4096\"\
]");

    auto tok = GameDataTokenizerFGD(ss);
    tok.SetErrorHandler([] (const std::string& message, int line) {
        std::cout << "ERROR on line " << line << " : " << message << std::endl;
    });

    auto gameDataClass = GameDataClass("SolidClass");
    auto result = gameDataClass.ReadFGD(tok);

    REQUIRE(result == true);
}
