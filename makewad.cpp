/*
 * The contents of this file are copyright 2003 Jedediah Smith
 * <jedediah@silencegreys.com>
 * http://extension.ws/hlfix/
 *
 * This work is licensed under the Creative Commons "Attribution-Share Alike 3.0 Unported" License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/legalcode
 * or, send a letter to Creative Commons, 171 2nd Street, Suite 300, San Francisco, California, 94105, USA.
*/

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <windows.h>

using namespace std;

LONG GetDWORDRegKey(HKEY hKey, const std::wstring &strValueName, DWORD &nValue, DWORD nDefaultValue)
{
    nValue = nDefaultValue;
    DWORD dwBufferSize(sizeof(DWORD));
    DWORD nResult(0);
    LONG nError = ::RegQueryValueExW(hKey,
        strValueName.c_str(),
        0,
        NULL,
        reinterpret_cast<LPBYTE>(&nResult),
        &dwBufferSize);
    if (ERROR_SUCCESS == nError)
    {
        nValue = nResult;
    }
    return nError;
}

LONG GetStringRegKey(HKEY hKey, const std::wstring &strValueName, std::wstring &strValue, const std::wstring &strDefaultValue)
{
    strValue = strDefaultValue;
    WCHAR szBuffer[512];
    DWORD dwBufferSize = sizeof(szBuffer);
    ULONG nError;
    nError = RegQueryValueExW(hKey, strValueName.c_str(), 0, NULL, (LPBYTE)szBuffer, &dwBufferSize);
    if (ERROR_SUCCESS == nError)
    {
        strValue = szBuffer;
    }
    return nError;
}

int main(int argc, char **argv)
{
    std::string outputFile = "wad.txt";
    std::vector<std::wstring> wadFiles;

    if (argc > 1) outputFile = argv[1];

    std::cout << "Writing to " << outputFile << std::endl;

    HKEY hKey;
    LONG lRes = RegOpenKeyExW(HKEY_CURRENT_USER, L"SOFTWARE\\Valve\\Valve Hammer Editor\\General", 0, KEY_READ, &hKey);

    if (lRes == ERROR_SUCCESS)
    {
        DWORD wadCount = 0;
        if (ERROR_SUCCESS == GetDWORDRegKey(hKey, L"TextureFileCount", wadCount, 0))
        {
            for (DWORD i = 0; i < wadCount; i++)
            {
                std::wstring strKeyValue;
                std::wstringstream wss;
                wss << L"TextureFile" << i;
                if (ERROR_SUCCESS == GetStringRegKey(hKey, wss.str(), strKeyValue, L"bad"))
                {
                    wadFiles.push_back(strKeyValue);
                }
            }
        }
    }
    else
    {
        std::wcerr << L"Registry key \"SOFTWARE\\Valve\\Valve Hammer Editor\\General\" does not exist." << std::endl;
    }

    if (!wadFiles.empty())
    {
        std::wofstream f(outputFile.c_str());
        for (auto wadFile : wadFiles)
        {
            std::wcout << wadFile << std::endl;
            f << wadFile << std::endl;
        }
        f.close();
    }

	return 0;
}
