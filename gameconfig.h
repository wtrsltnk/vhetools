#ifndef GAMECONFIG_H
#define GAMECONFIG_H

#include <string>
#include <vector>
#include <fstream>
#include <cstdarg>

#define STRING_SIZE 128

enum class GameConfigMapFormats
{
    Quake = 0,
    Hexen2,
    Quake2,
    HalfLife,
    HalfLife2,
    TeamFortress2,
};

enum class GameConfigTextureFormats
{
    None = -1,
    WAD = 0,
    WAL = 1,
    WAD3 = 2,
    WAD4 = 3,
    WAD5 = 4,
    VMT = 5,
    Sprite = 6
};

class GameConfigException
{
public:
    char *msg;

    GameConfigException(const char *tomsg, ...);
    virtual ~GameConfigException();
};

class GameConfig
{
    char _name[STRING_SIZE];
    GameConfigMapFormats _mapFormat;
    GameConfigTextureFormats _textureFormat;
    char _paletteFile[STRING_SIZE];

    char _defaultSolidEntity[STRING_SIZE];
    char _defaultPointEntity[STRING_SIZE];

    char _gameExe[STRING_SIZE];
    char _gameExeDir[STRING_SIZE];
    char _modDir[STRING_SIZE];
    char _baseDir[STRING_SIZE];

    char _rmfDir[STRING_SIZE];
    char _bspDir[STRING_SIZE];

    char _bspExe[STRING_SIZE];
    char _radExe[STRING_SIZE];
    char _visExe[STRING_SIZE];
    char _csgExe[STRING_SIZE];

    std::vector<std::string> _fgdFiles;

public:
    GameConfig();
    virtual ~GameConfig();

public:
    const char* name() const;
    void setName(const std::string& name);

    GameConfigMapFormats mapFormat() const;
    void setMapFormat(GameConfigMapFormats mapFormat);

    GameConfigTextureFormats textureFormat() const;
    void setTextureFormat(GameConfigTextureFormats textureFormat);

    const char* paletteFile() const;
    void setPaletteFile(const std::string& paletteFile);

    const char* defaultSolidEntity() const;
    void setDefaultSolidEntity(const std::string& defaultSolidEntity);

    const char* defaultPointEntity() const;
    void setDefaultPointEntity(const std::string& defaultPointEntity);

    const char* gameExe() const;
    void setGameExe(const std::string& gameExe);

    const char* gameExeDir() const;
    void setGameExeDir(const std::string& gameExeDir);

    const char* modDir() const;
    void setModDir(const std::string& modDir);

    const char* baseDir() const;
    void setBaseDir(const std::string& baseDir);

    const char* rmfDir() const;
    void setRmfDir(const std::string& rmfDir);

    const char* bspDir() const;
    void setBspDir(const std::string& bspDir);

    const char* bspExe() const;
    void setBspExe(const std::string& bspExe);

    const char* radExe() const;
    void setRadExe(const std::string& radExe);

    const char* visExe() const;
    void setVisExe(const std::string& visExe);

    const char* csgExe() const;
    void setCsgExe(const std::string& csgExe);

    const std::vector<std::string>& fgdFiles() const;
    void addFgdFile(const std::string& fgdFile);
    void removeFgdFile(const std::string& fgdFile);

public:
    static std::vector<GameConfig*> ReadGameConfigFile(const std::string& filename);
    static GameConfig* ReadGameConfig(std::ifstream& file, float version);

    static void WriteGameConfigFile(const char *filename, const std::vector<GameConfig*>& configs, float version);
    static void WriteGameConfig(std::ofstream& file, const GameConfig* config, float version);
};

#endif // GAMECONFIG_H
