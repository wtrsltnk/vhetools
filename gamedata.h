#ifndef GAMEDATA_H
#define GAMEDATA_H

#include <vector>
#include <string>
#include <functional>

class GameData
{
public:
    GameData();
    virtual ~GameData();

    std::vector<class GameDataClass*> _classes;

    bool ReadFGD(class GameDataTokenizerFGD& tok);
};

#endif // GAMEDATA_H
