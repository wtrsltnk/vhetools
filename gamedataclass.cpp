#include "gamedataclass.h"
#include "gamedataproperty.h"
#include "gamedatatokenizerfgd.h"
#include "utils.h"

GameDataClass::GameDataClass(const std::string& type)
    : _isSprite(false), _isStudio(false), _isDecal(false)
{
    this->_classType = GameDataClass::TypeFromString(type);
}

GameDataClass::~GameDataClass()
{ }

bool GameDataClass::ReadFGD(GameDataTokenizerFGD& tok)
{
    std::string token;
    auto ttype = tok.NextToken(token);
    std::vector<std::string> parameters;

    while (ttype == GameDataTokenTypes::Identifier)
    {
        if (CaseInsensitiveCompare(token, "base"))
        {
            if (!ReadParameterListFromFGD(tok, parameters)) return false;
            this->_inherited = parameters;
        }
        else if (CaseInsensitiveCompare(token, "size"))
        {
            this->ReadEditorSizeFromFGD(tok);
        }
        else if (CaseInsensitiveCompare(token, "color"))
        {
            this->ReadEditorColorFromFGD(tok);
        }
        else if (CaseInsensitiveCompare(token, "iconsprite"))
        {
            if (!ReadParameterListFromFGD(tok, parameters)) return false;
            if (parameters.size() != 1) return tok.Error("Wrong number of parameters for iconsprite, expected just 1");
            this->_editorIconSprite = parameters[0];
        }
        else if (CaseInsensitiveCompare(token, "sprite"))
        {
            if (!ReadParameterListFromFGD(tok, parameters)) return false;
            this->_isSprite = true;
        }
        else if (CaseInsensitiveCompare(token, "studio"))
        {
            if (!ReadParameterListFromFGD(tok, parameters)) return false;
            this->_isStudio = true;
        }
        else if (CaseInsensitiveCompare(token, "decal"))
        {
            if (!ReadParameterListFromFGD(tok, parameters)) return false;
            this->_isDecal = true;
        }
        else return tok.Error("Unknown identifier " + token);

        ttype = tok.NextToken(token);
    }

    if (ttype != GameDataTokenTypes::Operator && token != "=") return tok.Error("Unexpected token '" + token + "', expected '='");

    if (!tok.NextTokenCheckType(GameDataTokenTypes::Identifier, token)) return false;

    this->_name = token;

    if (tok.NextTokenCheck(GameDataTokenTypes::Operator, ":", true))
    {
        if (!tok.NextTokenCheckType(GameDataTokenTypes::String, token)) return false;
        this->_description = token;
    }

    if (!tok.NextTokenCheck(GameDataTokenTypes::Operator, "[")) return false;

    ttype = tok.NextToken(token);
    while (!(ttype == GameDataTokenTypes::Operator && token == "]"))
    {
        if (ttype != GameDataTokenTypes::Identifier) return tok.Error("Unexpected token '" + token + "', expected an Identifier");

        auto property = new GameDataProperty(token);
        if (!property->ReadPropertyFromFGD(tok))
        {
            delete property;
            return false;
        }

        this->_properties.push_back(property);

        ttype = tok.NextToken(token);
    }

    return true;
}

bool GameDataClass::ReadEditorSizeFromFGD(GameDataTokenizerFGD& tok)
{
    std::vector<std::string> parameters;
    if (!ReadParameterListFromFGD(tok, parameters)) return false;

    if (parameters.size() < 1 || parameters.size() > 2) return tok.Error("Wrong number of parameters for size, expected 1 or 2");

    if (parameters.size() == 1)
    {
        int vec[3];
        sscanf(parameters[0].c_str(), "%d %d %d", &vec[0], &vec[1], &vec[2]);
        for (int i = 0; i < 3; i++)
        {
            this->_editorSize.mins[i] = -(vec[i] / 2);
            this->_editorSize.maxs[i] = (vec[i] / 2);
        }
    }
    else
    {
        sscanf(parameters[0].c_str(), "%d %d %d", &this->_editorSize.mins[0], &this->_editorSize.mins[1], &this->_editorSize.mins[2]);
        sscanf(parameters[1].c_str(), "%d %d %d", &this->_editorSize.maxs[0], &this->_editorSize.maxs[1], &this->_editorSize.maxs[2]);
    }

    return true;
}

bool GameDataClass::ReadEditorColorFromFGD(GameDataTokenizerFGD& tok)
{
    std::vector<std::string> parameters;
    if (!ReadParameterListFromFGD(tok, parameters)) return false;

    if (parameters.size() != 1) return tok.Error("Wrong number of parameters for size, expected just 1");

    sscanf(parameters[0].c_str(), "%d %d %d", &this->_editorColor.rgb[0], &this->_editorColor.rgb[1], &this->_editorColor.rgb[2]);

    return true;
}

bool GameDataClass::ReadParameterListFromFGD(class GameDataTokenizerFGD& tok, std::vector<std::string>& params)
{
    params.clear();

    std::string token;
    if (!tok.NextTokenCheck(GameDataTokenTypes::Operator, "(")) return false;

    std::string param;
    auto ttype = tok.NextToken(token);
    while (!(ttype == GameDataTokenTypes::Operator && token == ")")
           && ttype != GameDataTokenTypes::EndOfFile
           && ttype != GameDataTokenTypes::Error)
    {
        if (ttype == GameDataTokenTypes::Operator && token == ",")
        {
            params.push_back(param);
            param = "";
        }
        else
        {
            // Chain all tokens for this parameter together with spaces
            param += " " + token;
            param = trim(param);
        }

        ttype = tok.NextToken(token);
    }

    // Dont forget to push the last param
    if (param != "") params.push_back(param);

    return true;
}

GameDataClassTypes GameDataClass::TypeFromString(const std::string& type)
{
    if (CaseInsensitiveCompare(type, "baseclass")) return GameDataClassTypes::BaseClass;
    if (CaseInsensitiveCompare(type, "solidclass")) return GameDataClassTypes::SolidClass;
    if (CaseInsensitiveCompare(type, "pointclass")) return GameDataClassTypes::PointClass;

    return GameDataClassTypes::BaseClass;
}
