#include "gamedatatokenizerfgd.h"

const std::string operatorCharacters = "@,!+&*$.=:[](){}\\";

GameDataTokenizerFGD::GameDataTokenizerFGD(std::istream& stream)
    : _stream(stream), _error([] (const std::string&, int) { }),
      _line(1), _lastType(GameDataTokenTypes::String)
{ }

GameDataTokenizerFGD::~GameDataTokenizerFGD() { }

GameDataTokenTypes GameDataTokenizerFGD::NextToken(std::string& token)
{
    token.clear();

    if (!this->_stash.empty())
    {
        auto t = this->_stash.front();
        this->_stash.pop();
        token = t._token;
        return t._type;
    }

    SkipWhiteSpace();

    if (this->_stream.eof())
    {
        this->_lastType = GameDataTokenTypes::EndOfFile;
        return this->_lastType;
    }

    char c = this->_stream.get();

    // Try parsing operator
    if (operatorCharacters.find(c) != std::string::npos)
    {
        token += c;
        this->_lastType = GameDataTokenTypes::Operator;
        return this->_lastType;
    }

    if (c == '\"')
    {
        return GetStringToken(token);
    }

    // Try parsing number
    if (std::isdigit(c) || c == '-')
    {
        do
        {
            token += c;
        } while (std::isdigit(c = this->_stream.get()));

        // It is not allowed for any identifier charater to follow a number
        if (std::isalpha(c) || c == '_')
        {
            token = "Invalid Identifier starting as a Number";
            this->_lastType = GameDataTokenTypes::Error;
            return this->_lastType;
        }

        this->_stream.putback(c);

        this->_lastType = GameDataTokenTypes::Number;
        return this->_lastType;
    }

    // Try parsing identifier
    while (std::isalpha(c) || std::isdigit(c) || c == '_')
    {
        token += c;
        c = this->_stream.get();
    }

    this->_stream.putback(c);

    this->_lastType = GameDataTokenTypes::Identifier;
    return this->_lastType;
}

std::string GetTokenTypeString(GameDataTokenTypes type)
{
    switch (type)
    {
    case GameDataTokenTypes::EndOfFile: return "End of file";
    case GameDataTokenTypes::Error: return "Error";
    case GameDataTokenTypes::Identifier: return "Identifier";
    case GameDataTokenTypes::Number: return "Number";
    case GameDataTokenTypes::Operator: return "Operator";
    case GameDataTokenTypes::String: return "String";
    }

    return "String";
}

bool GameDataTokenizerFGD::NextTokenCheck(GameDataTokenTypes checkType, const std::string& checkToken, bool stashOnFalse)
{
    std::string token;
    auto ttype = this->NextToken(token);

    if (ttype != checkType)
    {

        if (stashOnFalse)
        {
            this->StashToken(ttype, token);
        }
        else
        {
            this->Error("Unexpected token '" + GetTokenTypeString(ttype) + "', expected '" + GetTokenTypeString(checkType) + "'");
        }
        return false;
    }

    if (token != checkToken)
    {
        if (stashOnFalse)
        {
            this->StashToken(ttype, token);
        }
        else
        {
            this->Error("Unexpected token '" + token + "', expected '" + checkToken + "'");
        }
        return false;
    }

    return true;
}

bool GameDataTokenizerFGD::NextTokenCheckType(GameDataTokenTypes checkType, std::string& token)
{
    auto ttype = this->NextToken(token);

    if (ttype != checkType) return this->Error("Unexpected token '" + GetTokenTypeString(ttype) + "', expected '" + GetTokenTypeString(checkType) + "'");

    return true;
}

GameDataTokenTypes GameDataTokenizerFGD::GetStringToken(std::string& token)
{
    char c = this->_stream.get();

    if (this->_stream.eof())
    {
        token = "Unexpected end of file while reading a String";
        this->_lastType = GameDataTokenTypes::Error;
        return this->_lastType;
    }

    while (c != '\"')
    {
        if (c == '\n') this->_line++;
        token += c;
        c = this->_stream.get();
        if (this->_stream.eof())
        {
            token = "Unexpected end of file while reading a String";
            this->_lastType = GameDataTokenTypes::Error;
            return this->_lastType;
        }
    }

    this->_lastType = GameDataTokenTypes::String;
    return this->_lastType;
}

void GameDataTokenizerFGD::SkipWhiteSpace()
{
    while (!this->_stream.eof())
    {
        char c = this->_stream.get();

        if (this->_stream.eof()) break;

        if ((c == ' ') || (c == '\t') || (c == '\r') || (c == 0)) continue;

        if (c == '\n')
        {
            this->_line++;
            continue;
        }

        if (c == '/' && this->_stream.peek() == '/')
        {
            SkipCommentLine();
            continue;
        }

        this->_stream.putback(c);
        break;
    }
}

void GameDataTokenizerFGD::SkipCommentLine()
{
    while (true)
    {
        char c = this->_stream.get();

        if (this->_stream.eof()) break;

        if (c == '\n')
        {
            this->_line++;
            break;
        }
    }
}

void GameDataTokenizerFGD::SkipUntill(const char a)
{
    while (true)
    {
        char c = this->_stream.get();

        if (this->_stream.eof()) break;

        if (c == '\n') this->_line++;

        if (c == a)
        {
            this->_stream.putback(c);
            break;
        }
    }
}

bool GameDataTokenizerFGD::EndOfLine()
{
    return this->_lastType == GameDataTokenTypes::EndOfFile;
}

void GameDataTokenizerFGD::SetErrorHandler(std::function<void (const std::string&, int)> error)
{
    this->_error = error;
}

bool GameDataTokenizerFGD::Error(const std::string& error, bool result)
{
    this->_error(error, this->_line);

    return result;
}

StashedToken::StashedToken(GameDataTokenTypes type, const std::string& token) : _type(type), _token(token) { }

void GameDataTokenizerFGD::StashToken(GameDataTokenTypes type, const std::string& token)
{
    auto t = StashedToken(type, token);
    this->_stash.push(t);
}
