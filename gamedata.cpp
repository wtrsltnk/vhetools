#include "gamedata.h"
#include "gamedatatokenizerfgd.h"
#include "gamedataclass.h"

#include <fstream>

GameData::GameData()
{ }

GameData::~GameData()
{ }

bool GameData::ReadFGD(GameDataTokenizerFGD& tok)
{
    GameDataTokenTypes ttype;
    std::string token;

    while (!tok.EndOfLine())
    {
        ttype = tok.NextToken(token);
        if (ttype == GameDataTokenTypes::EndOfFile) return true;
        if (ttype != GameDataTokenTypes::Operator && token != "@") return tok.Error("Unexpected token '" + token + "', expected '@'");
        if (!tok.NextTokenCheckType(GameDataTokenTypes::Identifier, token)) return false;

        if (CaseInsensitiveCompare(token, "baseclass")
                || CaseInsensitiveCompare(token, "pointclass")
                || CaseInsensitiveCompare(token, "solidclass"))
        {
            auto gameDataClass = new GameDataClass(token);
            if (!gameDataClass->ReadFGD(tok))
            {
                 tok.Error("Cannot find include file " + token);
                 tok.SkipUntill('@');
                 delete gameDataClass;
                 continue;
            }
            this->_classes.push_back(gameDataClass);
        }
        else if (CaseInsensitiveCompare(token, "include"))
        {
            if (tok.NextTokenCheckType(GameDataTokenTypes::String, token))
            {
                std::ifstream includeFile(token.c_str());
                if (includeFile.bad()) tok.Error("Cannot find include file " + token);

                auto includeTok = GameDataTokenizerFGD(includeFile);
                if (!this->ReadFGD(includeTok)) tok.Error("Error including " + token);
            }
        }
        else if (CaseInsensitiveCompare(token, "mapsize"))
        {
            // For now we skip this
            tok.SkipUntill('@');
        }
        else
        {
            tok.Error("Invalid identifier " + token);
            tok.SkipUntill('@');
        }
    }

    return true;
}
