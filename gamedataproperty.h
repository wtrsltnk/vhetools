#ifndef GAMEDATAPROPERTY_H
#define GAMEDATAPROPERTY_H

#include <map>
#include <set>
#include <string>

enum class GameDataPropertyTypes
{
    String,
    Integer,
    Flags,
    Choices,
    Color255,
    TargetSource,
    TargetDestination,
    Studio,
    Sprite,
    Decal,
    Sound,
};

class GameDataProperty
{
public:
    GameDataProperty(const std::string& name);
    virtual ~GameDataProperty();

    std::string _name;
    GameDataPropertyTypes _propertyType;
    std::string _description;
    std::set<int> _defaultInteger;
    std::string _defaultString;
    std::map<int, std::string> _options;

    bool ReadPropertyFromFGD(class GameDataTokenizerFGD& tok);
    static bool ReadValueListFromFGD(class GameDataTokenizerFGD& tok, std::map<int, std::string>& values, std::set<int>& defaults, GameDataPropertyTypes type);
    static GameDataPropertyTypes TypeFromString(const std::string& type);
};

#endif // GAMEDATAPROPERTY_H
