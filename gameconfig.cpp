#include "gameconfig.h"
#include <fstream>
#include <string.h>

const char GameConfigurationFileId[] = "Game Configurations File\r\n\x1a";
const float GameConfigurationFileVersion = 1.4f;

GameConfigException::GameConfigException(const char *tomsg, ...)
{
    va_list args;

    msg = new char[1000];

    va_start(args,tomsg);
    vsprintf(msg,tomsg,args);
    va_end(args);
}

GameConfigException::~GameConfigException()
{
    delete msg;
}

GameConfig::GameConfig()
    : _mapFormat(GameConfigMapFormats::Quake)
{
    this->_mapFormat = GameConfigMapFormats::Quake;
    this->_textureFormat = GameConfigTextureFormats::None;

    strncpy(this->_name, "", STRING_SIZE);
    strncpy(this->_paletteFile, "", STRING_SIZE);
    strncpy(this->_defaultSolidEntity, "", STRING_SIZE);
    strncpy(this->_defaultPointEntity, "", STRING_SIZE);
    strncpy(this->_gameExe, "", STRING_SIZE);
    strncpy(this->_gameExeDir, "", STRING_SIZE);
    strncpy(this->_modDir, "", STRING_SIZE);
    strncpy(this->_baseDir, "", STRING_SIZE);
    strncpy(this->_rmfDir, "", STRING_SIZE);
    strncpy(this->_bspDir, "", STRING_SIZE);
    strncpy(this->_bspExe, "", STRING_SIZE);
    strncpy(this->_radExe, "", STRING_SIZE);
    strncpy(this->_visExe, "", STRING_SIZE);
    strncpy(this->_csgExe, "", STRING_SIZE);
}

GameConfig::~GameConfig()
{ }

std::vector<GameConfig*> GameConfig::ReadGameConfigFile(const std::string& filename)
{
    std::ifstream file(filename.c_str(), std::ios::binary);

    if (!file.is_open()) throw new GameConfigException("file does not exist");

    // Read and check the file ID
    char id[sizeof(GameConfigurationFileId)];
    file.read(id, sizeof(GameConfigurationFileId));
    if (strcmp(id, GameConfigurationFileId) != 0) throw new GameConfigException("file is no game configuration file");

    // Read and check the file version
    float version = 0.0f;
    file.read((char*)&version, sizeof(version));
    if (version < 1.0 || version > GameConfigurationFileVersion) throw new GameConfigException("incorrect file version: %f", version);

    // Read the number of configurations in this file
    int configCount = 0;
    file.read((char*)&configCount, sizeof(configCount));

    // Read all game configurations
    std::vector<GameConfig*> result;
    for (int i = 0; i < configCount; i++)
    {
        result.push_back(GameConfig::ReadGameConfig(file, version));
    }

    return result;
}

GameConfig* GameConfig::ReadGameConfig(std::ifstream& file, float version)
{
    auto config = new GameConfig();

    file.read(config->_name, sizeof(config->_name));
    int fgdFileCount;
    file.read((char*)&fgdFileCount, sizeof(fgdFileCount));
    file.read((char*)&config->_textureFormat, sizeof(config->_textureFormat));

    if (version >= 1.1f) file.read((char*)&config->_mapFormat, sizeof(config->_mapFormat));
    if (version < 1.4f) file.read(config->_paletteFile, sizeof(config->_paletteFile));

    file.read(config->_gameExe, sizeof(config->_gameExe));
    file.read(config->_defaultSolidEntity, sizeof(config->_defaultSolidEntity));
    file.read(config->_defaultPointEntity, sizeof(config->_defaultPointEntity));

    if (version >= 1.2f)
    {
        file.read(config->_bspExe, sizeof(config->_bspExe));
        file.read(config->_radExe, sizeof(config->_radExe));
        file.read(config->_visExe, sizeof(config->_visExe));
        file.read(config->_gameExeDir, sizeof(config->_gameExeDir));
        file.read(config->_rmfDir, sizeof(config->_rmfDir));
    }

    if (version >= 1.3f)
    {
        file.read(config->_bspDir, sizeof(config->_bspDir));
    }

    if (version >= 1.4f)
    {
        file.read(config->_csgExe, sizeof(config->_csgExe));
        file.read(config->_modDir, sizeof(config->_modDir));
        file.read(config->_baseDir, sizeof(config->_baseDir));
    }

    char dir[STRING_SIZE] = { 0 };
    for (int i = 0; i < fgdFileCount; i++)
    {
        file.read(dir, sizeof(dir));
        config->_fgdFiles.push_back(dir);
    }

    return config;
}

void GameConfig::WriteGameConfigFile(const char *filename, const std::vector<GameConfig*>& configs, float version)
{
    std::ofstream file(filename, std::ios::binary);

    if (!file.is_open()) throw new GameConfigException("unable to write to file");

    file.write(GameConfigurationFileId, sizeof(GameConfigurationFileId));
    file.write((char*)&version, sizeof(version));
    int configCount = configs.size();
    file.write((char*)&configCount, sizeof(configCount));

    for (auto config : configs) WriteGameConfig(file, config, version);
}

void GameConfig::WriteGameConfig(std::ofstream& file, const GameConfig* config, float version)
{
    file.write(config->_name, sizeof(config->_name));
    int fgdFileCount = config->_fgdFiles.size();
    file.write((char*)&fgdFileCount, sizeof(fgdFileCount));
    file.write((char*)&config->_textureFormat, sizeof(config->_textureFormat));

    if (version >= 1.1f) file.write((char*)&config->_mapFormat, sizeof(config->_mapFormat));
    if (version < 1.4f) file.write(config->_paletteFile, sizeof(config->_paletteFile));

    file.write(config->_gameExe, sizeof(config->_gameExe));
    file.write(config->_defaultSolidEntity, sizeof(config->_defaultSolidEntity));
    file.write(config->_defaultPointEntity, sizeof(config->_defaultPointEntity));

    if (version >= 1.2f)
    {
        file.write(config->_bspExe, sizeof(config->_bspExe));
        file.write(config->_radExe, sizeof(config->_radExe));
        file.write(config->_visExe, sizeof(config->_visExe));
        file.write(config->_gameExeDir, sizeof(config->_gameExeDir));
        file.write(config->_rmfDir, sizeof(config->_rmfDir));
    }

    if (version >= 1.3f)
    {
        file.write(config->_bspDir, sizeof(config->_bspDir));
    }

    if (version >= 1.4f)
    {
        file.write(config->_csgExe, sizeof(config->_csgExe));
        file.write(config->_modDir, sizeof(config->_modDir));
        file.write(config->_baseDir, sizeof(config->_baseDir));
    }

    for (auto fgdFile : config->_fgdFiles)
    {
        file.write(fgdFile.c_str(), sizeof(char) * STRING_SIZE);
    }

}

const char* GameConfig::name() const { return this->_name; }
void GameConfig::setName(const std::string& name) { strncpy(this->_name, name.c_str(), STRING_SIZE); }

GameConfigMapFormats GameConfig::mapFormat() const { return this->_mapFormat; }
void GameConfig::setMapFormat(GameConfigMapFormats mapFormat) { this->_mapFormat = mapFormat; }

GameConfigTextureFormats GameConfig::textureFormat() const { return this->_textureFormat; }
void GameConfig::setTextureFormat(GameConfigTextureFormats textureFormat) { this->_textureFormat = textureFormat; }

const char* GameConfig::paletteFile() const { return this->_paletteFile; }
void GameConfig::setPaletteFile(const std::string& paletteFile) { strncpy(this->_paletteFile, paletteFile.c_str(), STRING_SIZE); }

const char* GameConfig::defaultSolidEntity() const { return this->_defaultSolidEntity; }
void GameConfig::setDefaultSolidEntity(const std::string& defaultSolidEntity) { strncpy(this->_defaultSolidEntity, defaultSolidEntity.c_str(), STRING_SIZE); }

const char* GameConfig::defaultPointEntity() const { return this->_defaultPointEntity; }
void GameConfig::setDefaultPointEntity(const std::string& defaultPointEntity) { strncpy(this->_defaultPointEntity, defaultPointEntity.c_str(), STRING_SIZE); }

const char* GameConfig::gameExe() const { return this->_gameExe; }
void GameConfig::setGameExe(const std::string& gameExe) { strncpy(this->_gameExe, gameExe.c_str(), STRING_SIZE); }

const char* GameConfig::gameExeDir() const { return this->_gameExeDir; }
void GameConfig::setGameExeDir(const std::string& gameExeDir) { strncpy(this->_gameExeDir, gameExeDir.c_str(), STRING_SIZE); }

const char* GameConfig::modDir() const { return this->_modDir; }
void GameConfig::setModDir(const std::string& modDir) { strncpy(this->_modDir, modDir.c_str(), STRING_SIZE); }

const char* GameConfig::baseDir() const { return this->_baseDir; }
void GameConfig::setBaseDir(const std::string& baseDir) { strncpy(this->_baseDir, baseDir.c_str(), STRING_SIZE); }

const char* GameConfig::rmfDir() const { return this->_rmfDir; }
void GameConfig::setRmfDir(const std::string& rmfDir) { strncpy(this->_rmfDir, rmfDir.c_str(), STRING_SIZE); }

const char* GameConfig::bspDir() const { return this->_bspDir; }
void GameConfig::setBspDir(const std::string& bspDir) { strncpy(this->_bspDir, bspDir.c_str(), STRING_SIZE); }

const char* GameConfig::bspExe() const { return this->_bspExe; }
void GameConfig::setBspExe(const std::string& bspExe) { strncpy(this->_bspExe, bspExe.c_str(), STRING_SIZE); }

const char* GameConfig::radExe() const { return this->_radExe; }
void GameConfig::setRadExe(const std::string& radExe) { strncpy(this->_radExe, radExe.c_str(), STRING_SIZE); }

const char* GameConfig::visExe() const { return this->_visExe; }
void GameConfig::setVisExe(const std::string& visExe) { strncpy(this->_visExe, visExe.c_str(), STRING_SIZE); }

const char* GameConfig::csgExe() const { return this->_csgExe; }
void GameConfig::setCsgExe(const std::string& csgExe) { strncpy(this->_csgExe, csgExe.c_str(), STRING_SIZE); }

const std::vector<std::string>& GameConfig::fgdFiles() const { return this->_fgdFiles; }
void GameConfig::addFgdFile(const std::string& fgdFile) { this->_fgdFiles.push_back(fgdFile); }
void GameConfig::removeFgdFile(const std::string& fgdFile)
{
    for (auto itr = this->_fgdFiles.begin(); itr != this->_fgdFiles.end(); ++itr)
    {
        if (*itr == fgdFile)
        {
            this->_fgdFiles.erase(itr);
            break;
        }
    }
}
