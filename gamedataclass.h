#ifndef GAMEDATACLASS_H
#define GAMEDATACLASS_H

#include <vector>
#include <map>
#include <set>
#include <string>
#include <functional>

class GameDataEditorSize
{
public:
    int mins[3];
    int maxs[3];
};

class GameDataEditorColor
{
public:
    int rgb[3];
};

enum class GameDataClassTypes
{
    BaseClass,
    PointClass,
    SolidClass,
};

class GameDataClass
{
public:
    GameDataClass(const std::string& type);
    virtual ~GameDataClass();

    GameDataClassTypes _classType;
    GameDataEditorSize _editorSize;
    GameDataEditorColor _editorColor;
    std::string _editorIconSprite;
    std::vector<std::string> _inherited;
    std::string _name;
    std::string _description;
    std::vector<class GameDataProperty*> _properties;
    bool _isSprite;
    bool _isStudio;
    bool _isDecal;

    bool ReadFGD(class GameDataTokenizerFGD& tok);
    bool ReadEditorSizeFromFGD(class GameDataTokenizerFGD& tok);
    bool ReadEditorColorFromFGD(class GameDataTokenizerFGD& tok);
    static bool ReadParameterListFromFGD(class GameDataTokenizerFGD& tok, std::vector<std::string>& params);
    static GameDataClassTypes TypeFromString(const std::string& type);
};

#endif // GAMEDATACLASS_H
