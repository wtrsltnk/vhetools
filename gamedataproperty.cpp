#include "gamedataproperty.h"
#include "gamedataclass.h"
#include "gamedatatokenizerfgd.h"

#include <vector>

GameDataProperty::GameDataProperty(const std::string& name) : _name(name) { }

GameDataProperty::~GameDataProperty() { }

bool GameDataProperty::ReadPropertyFromFGD(class GameDataTokenizerFGD& tok)
{
    std::vector<std::string> parameters;

    if (!GameDataClass::ReadParameterListFromFGD(tok, parameters)) return false;
    if (parameters.size() != 1) return tok.Error("Wrong number of parameters for property type, expected just 1");
    this->_propertyType = GameDataProperty::TypeFromString(parameters[0]);

    std::string token;
    GameDataTokenTypes ttype;

    // Is there a description given?
    if (tok.NextTokenCheck(GameDataTokenTypes::Operator, ":", true))
    {
        // Read description
        if (!tok.NextTokenCheckType(GameDataTokenTypes::String, token)) return false;
        this->_description = token;

        // Is there a default value given?
        if (tok.NextTokenCheck(GameDataTokenTypes::Operator, ":", true))
        {
            ttype = tok.NextToken(token);
            if (ttype == GameDataTokenTypes::Number)
            {
                // TODO : check property type
                this->_defaultInteger.insert(atoi(token.c_str()));
            }
            else if (ttype == GameDataTokenTypes::String)
            {
                // TODO : check property type
                this->_defaultString = token;
            }
        }
    }

    if (this->_propertyType == GameDataPropertyTypes::Choices || this->_propertyType == GameDataPropertyTypes::Flags)
    {
        if (!tok.NextTokenCheck(GameDataTokenTypes::Operator, "=")) return false;

        GameDataProperty::ReadValueListFromFGD(tok, this->_options, this->_defaultInteger, this->_propertyType);
    }

    return true;
}

bool GameDataProperty::ReadValueListFromFGD(class GameDataTokenizerFGD& tok, std::map<int, std::string>& values, std::set<int>& defaults, GameDataPropertyTypes type)
{
    std::string token;
    GameDataTokenTypes ttype;

    if (!tok.NextTokenCheck(GameDataTokenTypes::Operator, "[")) return false;

    ttype = tok.NextToken(token);
    while (!(ttype == GameDataTokenTypes::Operator && token == "]"))
    {
        if (ttype != GameDataTokenTypes::Number) return tok.Error("Unexpected token '" + token + "', expected a Number");
        int index = atoi(token.c_str());

        if (!tok.NextTokenCheck(GameDataTokenTypes::Operator, ":")) return false;

        if (!tok.NextTokenCheckType(GameDataTokenTypes::String, token)) return false;

        values.insert(std::make_pair(index, token));

        if (type == GameDataPropertyTypes::Flags)
        {
            if (!tok.NextTokenCheck(GameDataTokenTypes::Operator, ":")) return false;
            if (!tok.NextTokenCheckType(GameDataTokenTypes::Number, token)) return false;

            defaults.insert(atoi(token.c_str()));
        }

        ttype = tok.NextToken(token);
    }

    return true;
}

GameDataPropertyTypes GameDataProperty::TypeFromString(const std::string& type)
{
    if (CaseInsensitiveCompare(type, "choices")) return GameDataPropertyTypes::Choices;
    else if (CaseInsensitiveCompare(type, "color255")) return GameDataPropertyTypes::Color255;
    else if (CaseInsensitiveCompare(type, "decal")) return GameDataPropertyTypes::Decal;
    else if (CaseInsensitiveCompare(type, "flags")) return GameDataPropertyTypes::Flags;
    else if (CaseInsensitiveCompare(type, "integer")) return GameDataPropertyTypes::Integer;
    else if (CaseInsensitiveCompare(type, "sound")) return GameDataPropertyTypes::Sound;
    else if (CaseInsensitiveCompare(type, "sprite")) return GameDataPropertyTypes::Sprite;
    else if (CaseInsensitiveCompare(type, "string")) return GameDataPropertyTypes::String;
    else if (CaseInsensitiveCompare(type, "studio")) return GameDataPropertyTypes::Studio;
    else if (CaseInsensitiveCompare(type, "targetdestination")) return GameDataPropertyTypes::TargetDestination;
    else if (CaseInsensitiveCompare(type, "targetsource")) return GameDataPropertyTypes::TargetSource;

    return  GameDataPropertyTypes::String;
}
